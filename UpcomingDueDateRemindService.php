<?php

declare(strict_types=1);

namespace App\FactoryNotification\Service;

use App\FactoryInformation\Repository\FactoryRepository;
use App\FactoryNotification\Repository\PlushieRepository;
use App\Factory\EmailRenderingClientFactoryInterface;
use App\Factory\EmailServiceFactory;
use App\Settings\Service\SettingReaderServiceInterface;
use App\Common\PlushieLinkBuilder;
use App\ProductionProcess\Entity\ProductionStatusInterface;
use BudsiesBackend\Api\CoreService\Declarations\PlushieInformation\ProductInterface;
use BudsiesBackend\Api\CoreService\Declarations\Settings\SettingCodeInterface;
use BudsiesBackend\Api\EmailRenderingService\Interfaces\TemplateCodeInterface;
use BudsiesBackend\Api\CoreService\Declarations\OrderInformation\StoreInterface;

class UpcomingDueDateRemindService
{
    private FactoryRepository $factoryRepository;
    private PlushieRepository $plushieRepository;
    private EmailRenderingClientFactoryInterface $emailRenderingClientFactory;
    private EmailServiceFactory $emailServiceFactory;
    private SettingReaderServiceInterface $settingReader;
    private PlushieLinkBuilder $plushieLinkBuilder;

    private const PRODUCT_LIST = [
        ProductInterface::BULK_SAMPLE,
        ProductInterface::PILLOW_BULK_SAMPLE,
    ];
    private const STATUS_LIST = ProductionStatusInterface::PRODUCTION_STATUS_LIST;

    public function __construct(
        FactoryRepository $factoryRepository,
        PlushieRepository $plushieRepository,
        EmailRenderingClientFactoryInterface $emailRenderingClientFactory,
        EmailServiceFactory $emailServiceFactory,
        SettingReaderServiceInterface $settingReader,
        PlushieLinkBuilder $plushieLinkBuilder
    )
    {
        $this->factoryRepository           = $factoryRepository;
        $this->plushieRepository           = $plushieRepository;
        $this->emailRenderingClientFactory = $emailRenderingClientFactory;
        $this->emailServiceFactory         = $emailServiceFactory;
        $this->settingReader               = $settingReader;
        $this->plushieLinkBuilder          = $plushieLinkBuilder;
    }

    public function remind(string $serviceToken): int
    {
        $countSentMessages = 0;

        $factories = $this->factoryRepository->findActive();
        if (!$factories) {
            return $countSentMessages;
        }

        $daysThreshold = (int)$this->settingReader->readValue(
            SettingCodeInterface::FACTORY_NOTIFICATION_UPCOMING_PLUSHIES_THRESHOLD
        );
        $additionalEmails = explode(';', $this->settingReader->readValue(
            SettingCodeInterface::FACTORY_NOTIFICATION_UPCOMING_PLUSHIES_CC_EMAILS
        ));

        $emailRenderingClient = $this->emailRenderingClientFactory->create($serviceToken);
        $emailService = $this->emailServiceFactory->create($serviceToken);

        foreach ($factories as $factory) {
            $plushies = $this->plushieRepository->findUpcomingForFactoryByProductAndStatus(
                self::PRODUCT_LIST,
                self::STATUS_LIST,
                $daysThreshold,
                $factory
            );

            if (!$plushies) {
                continue;
            }

            foreach ($plushies as &$plushie) {
                $plushie['url'] = $this->plushieLinkBuilder->getViewLink($plushie['id']);
            }

            $vars = [
                'factory_name' => $factory->getName(),
                'plushies' => $plushies
            ];

            $template = $emailRenderingClient->renderTemplate(
                StoreInterface::BUDSIES,
                TemplateCodeInterface::FACTORY_NOTIFY_ABOUT_UPCOMING_PLUSHIES,
                $vars
            );

            $emailService->sendEmail(
                [$factory->getEmail()],
                $template->getFromEmail(),
                $template->getSubject(),
                $template->getContent(),
                $template->getFromName(),
                [],
                $additionalEmails
            );

            $countSentMessages++;
        }

        return $countSentMessages;
    }
}